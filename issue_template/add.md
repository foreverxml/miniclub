---

name: "Add Website"
about: "Add your website to Miniclub!"
title: "Add example.com"

---

## My website...

- [ ] has a 100 on Accessibility in Lighthouse
- [ ] has a 100 on Best Practices in Lighthouse
- [ ] has a 90 or above on Performance in Lighthouse
- [ ] is open source
- [ ] has CSS that takes care of most configurations
- [ ] uses the Rule of Least Power
- [ ] keeps everything non-NSFW and free of any -isms
- [ ] is designed around Accessibility
- [ ] uses HTTPS
- [ ] links back to <https://forever.amongtech.cc/miniclub>

## What's your website?

- Website: `https://example.com`
- Git repo: `https://codeberg.org/...`
- My email: `person@amogmail.org`